<?php
    
    include_once("catalogoMetodo.php");
    $servico = filter_input(INPUT_POST, "servico", FILTER_SANITIZE_STRING);
    $catalogo = filter_input(INPUT_POST, "catalogo", FILTER_SANITIZE_STRING);
    $preco =(int) filter_input(INPUT_POST, "preco", FILTER_SANITIZE_STRING);
    
    if($servico && $catalogo){
        setServico($servico);
        setCatalogo($catalogo,$preco);
    }
    
?>
<!doctype html>
<html lang="en">
	<?php include_once("includes/header.php");?>
		<div class="container vertical-align">
			<div class="row justify-content-md-center">
			    
				<form action='addCatalogo.php' method='post'>
				    <?php
			            if($servico && $catalogo){
			                    echo "Você adicionou serviço com sucesso! <p> Pode adicionar outro a baixo </p>";
			            }
                    ?>
					<div class="form-group">
						Escreva serviço que quer adicionar:
						<input type="text" class="form-control" placeholder="Serviço" name='servico'>
		
					</div>
					<div class="form-group">
						Escreva categoria que quer adicionar:
						<input type="text" class="form-control" placeholder="Categoria" name='catalogo'>
					</div>
					
					<div class="form-group">
						Escreva Preço que quer adicionar:
						<input type="text" class="form-control" placeholder="preco" name='preco'>
					</div>
	
					<input type="submit" class="btn btn-primary" value="Entrar">
					
				</form>
			</div>
		</div>
		<?php include_once("includes/footer.php");?>
	
	</body>
</html>