<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Serviço Fácil</title>
</head>
<body>
<header class="border-bottom border-light">
	<div class="container pt-2">
		<div class="row">
					
			<div class="col">
				<h1><a href="index.php" class="text-dark">Serviço Fácil</a></h1>
			</div>
				
			<div class="col col-lg-2">
				<h5><a href="index.php" class="text-dark">Inicio</a></h5>
			</div>	
			<div class="col col-lg-2">
				<h5><a href="#" class="text-dark">Quem somos</a></h5>
			</div>	
			<div class="col col-lg-2">
				<h5><a href="#" class="text-dark">Termos de uso</a></h5>
			</div>			
		</div>
	</div>
</header>
