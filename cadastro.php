<!doctype html>
<html lang="en">
	<?php include_once("includes/header.php");?>
	<?php 	$ator = filter_input(INPUT_GET, "ator", FILTER_SANITIZE_STRING); ?>
		<div class="container vertical-align">
			<div class="row justify-content-md-center">
				<form action='validarCadastro.php' method='post'>
					<h3>Cadastro de <?php echo $ator; ?></h3>
					<div class="form-group">
						Nome
						<input type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp" placeholder="Nome" name='nome'>				
					</div>

					<div class="form-group">
						Endereco de email
						<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" name='email'>
		
					</div>
					<div class="form-group">
						Senha
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha" name='senha_01'>
					</div>
					<div class="form-group">
						Redigite
						<input type="password" class="form-control" id="exampleInputPassword2" placeholder="Senha" name='senha_02'>
					</div>
	                <input type="hidden" value="<?php echo $ator; ?>" name="ator">
					<input type="submit" class="btn btn-primary my-2" value="Cadastrar">
				</form>

			</div>
		</div>
		<?php include_once("includes/footer.php");?>
	</body>
</html>
