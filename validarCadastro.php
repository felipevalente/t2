<?php
	$nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
	$ator = filter_input(INPUT_POST, "ator", FILTER_SANITIZE_STRING);
	$senha_01 = filter_input(INPUT_POST, "senha_01", FILTER_SANITIZE_STRING);
	$senha_02 = filter_input(INPUT_POST, "senha_02", FILTER_SANITIZE_STRING);
	$headers = 'Content-type: text/html; charset=utf-8'."\r\n";
	
 	if($senha_01 == $senha_02) {
		session_start();
		$usuario_valido = false;
		$usuario = [$nome, $email, $ator, $senha_01, $usuario_valido]; 
		$_SESSION['usuario'] = $usuario;

		$msg = "<a href='https://felipehostinger.000webhostapp.com/T2/T2/T2/confirmarEmail.php
?ator=$ator'>Clique aqui para validar sua conta</a>";
		mail($email, 'Serviço Fácil', $msg, $headers);
		header('location: index.php');
		exit(0);
	}
	else 
		echo 'Senhas diferentes'
		     .'<br>'
		     . '<a href="index.php">Voltar</a>';

	
